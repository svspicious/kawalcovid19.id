import * as React from 'react';

const InfographicIcon: React.FC<React.SVGProps<SVGSVGElement>> = ({
  width = 16,
  height = 17,
  fill = '#f1f2f3',
  ...rest
}) => {
  return (
    <svg width={width} height={height} viewBox="0 0 16 14" fill="none" {...rest}>
      <path
        d="M15.625 11H1V0.375C1 0.1875 0.8125 0 0.625 0H0.375C0.15625 0 0 0.1875 0 0.375V11.625C0 11.8438 0.15625 12 0.375 12H15.625C15.8125 12 16 11.8438 16 11.625V11.375C16 11.1875 15.8125 11 15.625 11ZM11.625 3.0625L9 4.75L6.28125 2.125C6.125 1.96875 5.875 1.96875 5.71875 2.15625L2 7V10H15L12.1562 3.25C12.0625 3.03125 11.8125 2.9375 11.625 3.0625ZM3 7.375L6.0625 3.25L8.875 6.0625L11.5312 4.25L13.4688 9H3V7.375Z"
        fill={fill}
      />
    </svg>
  );
};

export default InfographicIcon;
