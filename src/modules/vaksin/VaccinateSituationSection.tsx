import * as React from 'react';
import useSWR from 'swr/dist';
import styled from '@emotion/styled';
import * as Sentry from '@sentry/browser';

import { Box, Heading, themeProps, Text, Stack, DataCard } from 'components/design-system';
import { fetch } from 'utils/api';
import { CEKDIRI_API_URL, VaksinResponse, VaksinStats } from 'types/cekdiri';
import formatTime from 'utils/formatTime';

const ERROR_TITLE = 'Error - Gagal mengambil data terbaru';
const ERROR_MESSAGE = 'Gagal mengambil data. Mohon coba lagi dalam beberapa saat.';

const GridWrapper = styled(Box)`
  display: grid;
  grid-template-rows: repeat(auto-fill, 1fr);
  grid-gap: ${themeProps.space.md}px;

  ${themeProps.mediaQueries.sm} {
    grid-template-columns: repeat(2, 1fr);
  }
`;

export interface PropgressBoxProps {
  color: string;
  value?: number;
}

export interface VaccinateSectionBlockProps {
  data?: VaksinStats;
  error: boolean;
  timestamp?: Date;
}

const VaccinateSectionBlock: React.FC<VaccinateSectionBlockProps> = React.memo(
  ({ data, error, timestamp }) => {
    if (error) {
      Sentry.withScope(scope => {
        scope.setTag('api_error', 'situasi_vaksinasi');
        Sentry.captureException(error);
      });
    }
    const SectionHeading = styled(Heading)`
      margin-bottom: ${themeProps.space.md}px;

      ${themeProps.mediaQueries.md} {
        margin-bottom: ${themeProps.space.xl}px;
      }
    `;

    const DetailsWrapper = styled(Box)`
      ${themeProps.mediaQueries.md} {
        display: flex;
        align-items: flex-end;
        justify-content: space-between;
      }
    `;

    return (
      <Stack mb="xxl" mt={50}>
        <SectionHeading variant={800} as="h2">
          Situasi Vaksinasi COVID-19 Saat Ini
        </SectionHeading>

        <Box>
          <GridWrapper>
            <DataCard
              color="success02"
              value={data?.total_sasaran_vaksinasi}
              label="Total Sasaran Vaksinasi"
            />
            <DataCard
              color="warning02"
              value={data?.sasaran_vaksinasi_sdmk}
              label="Sasaran Vaksinasi Tenaga Kesahatan"
            />
            <DataCard
              color="highlight03"
              value={data?.sasaran_vaksinasi_petugas_publik}
              label="Sasaran Vaksinasi Petugas Publik"
            />
            <DataCard
              color="primary02"
              value={data?.sasaran_vaksinasi_lansia}
              label="Sasaran Vaksinasi Lansia"
            />
          </GridWrapper>
          <DetailsWrapper>
            <Box mt="md">
              <Text as="h5" m={0} variant={200} color="accents04" fontWeight={400}>
                {error ? ERROR_TITLE : 'Pembaruan Terakhir'}
              </Text>
              <Text as="p" variant={400} color="accents07" fontFamily="monospace">
                {timestamp ? formatTime(new Date(timestamp), 'longest') : ERROR_MESSAGE}
              </Text>
            </Box>
          </DetailsWrapper>
        </Box>
      </Stack>
    );
  }
);

const VaccinateSection: React.FC = () => {
  const { data, error } = useSWR<VaksinResponse | undefined>(CEKDIRI_API_URL, fetch);
  const clear = data?.monitoring[data.monitoring.length - 1];
  return <VaccinateSectionBlock data={clear} timestamp={data?.last_updated} error={!!error} />;
};

export default VaccinateSection;
