import * as React from 'react';
import useSWR from 'swr/dist';
import styled from '@emotion/styled';
import * as Sentry from '@sentry/browser';

import { Box, Heading, themeProps, Text, Stack } from 'components/design-system';
import { fetch } from 'utils/api';
import { CEKDIRI_API_URL, VaksinResponse, VaksinStats } from 'types/cekdiri';
import formatNumber from 'utils/formatNumber';
import formatTime from 'utils/formatTime';

const EMPTY_DASH = '----';
const ERROR_TITLE = 'Error - Gagal mengambil data terbaru';
const ERROR_MESSAGE = 'Gagal mengambil data. Mohon coba lagi dalam beberapa saat.';

const GridWrapper = styled(Box)`
  display: grid;
  grid-template-rows: repeat(auto-fill, 1fr);
  grid-gap: ${themeProps.space.md}px;

  ${themeProps.mediaQueries.sm} {
    grid-template-columns: repeat(2, 1fr);
  }
`;

const BoxWrapper = styled(Box)`
  display: grid;
  grid-template-rows: repeat(auto-fill, 1fr);
  grid-gap: ${themeProps.space.md}px;

  ${themeProps.mediaQueries.sm} {
    grid-template-columns: repeat(1, 1fr);
  }
`;

const ProgressWrapper = styled(Box)`
  display: flex;
  flex-direction: row;
  gap: ${themeProps.space.md}px;

  ${themeProps.mediaQueries.sm} {
    grid-template-columns: repeat(2, 1fr);
  }
`;

export interface VaksinasiProps {
  color: string;
  value?: number;
  judul: string;
  percentage?: number;
  total?: number;
  divaksin?: number;
  tertunda?: number;
}

const Vaksinasi: React.FC<VaksinasiProps> = ({
  color,
  judul,
  value,
  percentage,
  total,
  divaksin,
  tertunda,
}) => {
  return (
    <Box px="xl" pt="xl" pb="xl" borderRadius={6} backgroundColor="card">
      <Text display="block" color="accents08" variant={700}>
        {judul}
      </Text>
      <Text
        textAlign="right"
        display="block"
        variant={1100}
        color={color}
        fontFamily="monospace"
        mt="sm"
      >
        {formatNumber(value) || EMPTY_DASH}
      </Text>
      <Text display="block" variant={400} color="accents06" textAlign="right">
        Dosis telah diberikan
      </Text>
      <ProgressWrapper mt="sm">
        <Box display="flex" flex={1} borderRadius={6} backgroundColor="progressBar">
          <Box height={30} borderRadius={6} backgroundColor={color} width={`${percentage}%`} />
        </Box>
        <Box display="flex" width={100} alignItems="center" justifyContent="flex-end">
          <Text fontFamily="monospace" variant={500} color={color}>
            {percentage ? formatNumber(percentage) : EMPTY_DASH} %
          </Text>
        </Box>
      </ProgressWrapper>
      <Text display="block" mt="sm" variant={400} color="accents06">
        {percentage ? formatNumber(percentage) : EMPTY_DASH}% dari {formatNumber(total)} telah
        divaksin
      </Text>
      {judul !== 'Semua Vaksinasi' && (
        <>
          <Box display="flex" flexDirection="row" justifyContent="space-between" mt="sm">
            <Text variant={500} color="accents07">
              Sudah divaksin
            </Text>
            <Text variant={500} color="success02">
              {divaksin ? formatNumber(divaksin) : EMPTY_DASH}
            </Text>
          </Box>
          <Box display="flex" flexDirection="row" justifyContent="space-between" mt="xs">
            <Text variant={500} color="accents07">
              Tertunda
            </Text>
            <Text variant={500} color="error02">
              {tertunda ? formatNumber(tertunda) : EMPTY_DASH}
            </Text>
          </Box>
        </>
      )}
    </Box>
  );
};

export interface ProgressBoxProps {
  data?: VaksinStats;
  tahap: number;
}
const percentage = (item: number, sasaran: number) => {
  const result = (item / sasaran) * 100;
  return result > 100 ? 100 : result;
};

const ProgressBox: React.FC<ProgressBoxProps> = React.memo(({ data, tahap }) => (
  <Box display="flex" alignItems="flex-start" justifyContent="flex-start">
    <Box textAlign="start" display="flex" flexDirection="column" flex={1}>
      <Text display="block" color="accents08" variant={700}>
        Vaksinasi Tahap {tahap}
      </Text>
      <BoxWrapper mt="md">
        <Vaksinasi
          color="success02"
          judul="Semua Vaksinasi"
          percentage={
            data
              ? percentage(
                  data[tahap === 1 ? 'vaksinasi1' : 'vaksinasi2'],
                  data.total_sasaran_vaksinasi
                )
              : 0
          }
          value={data ? data[tahap === 1 ? 'vaksinasi1' : 'vaksinasi2'] : 0}
          total={data?.total_sasaran_vaksinasi}
        />

        <Vaksinasi
          color="warning02"
          judul="Vaksinasi Tenaga Kesehatan"
          percentage={
            data
              ? percentage(
                  data?.tahapan_vaksinasi.sdm_kesehatan[
                    tahap === 1 ? 'total_vaksinasi1' : 'total_vaksinasi2'
                  ],
                  data?.sasaran_vaksinasi_sdmk
                )
              : 0
          }
          value={
            data?.tahapan_vaksinasi.sdm_kesehatan[
              tahap === 1 ? 'total_vaksinasi1' : 'total_vaksinasi2'
            ]
          }
          total={data?.sasaran_vaksinasi_sdmk}
          divaksin={
            data?.tahapan_vaksinasi.sdm_kesehatan[tahap === 1 ? 'sudah_vaksin1' : 'sudah_vaksin2']
          }
          tertunda={
            data?.tahapan_vaksinasi.sdm_kesehatan[
              tahap === 1 ? 'tertunda_vaksin1' : 'tertunda_vaksin2'
            ]
          }
        />

        <Vaksinasi
          color="highlight03"
          judul="Vaksinasi Petugas Publik"
          percentage={
            data
              ? percentage(
                  data?.tahapan_vaksinasi.petugas_publik[
                    tahap === 1 ? 'total_vaksinasi1' : 'total_vaksinasi2'
                  ],
                  data?.sasaran_vaksinasi_petugas_publik
                )
              : 0
          }
          value={
            data?.tahapan_vaksinasi.petugas_publik[
              tahap === 1 ? 'total_vaksinasi1' : 'total_vaksinasi2'
            ]
          }
          total={data?.sasaran_vaksinasi_petugas_publik}
          divaksin={
            data?.tahapan_vaksinasi.petugas_publik[tahap === 1 ? 'sudah_vaksin1' : 'sudah_vaksin2']
          }
          tertunda={
            data?.tahapan_vaksinasi.petugas_publik[
              tahap === 1 ? 'tertunda_vaksin1' : 'tertunda_vaksin2'
            ]
          }
        />
        <Vaksinasi
          color="primary02"
          judul="Vaksinasi Lansia"
          percentage={
            data
              ? percentage(
                  data?.tahapan_vaksinasi.lansia[
                    tahap === 1 ? 'total_vaksinasi1' : 'total_vaksinasi2'
                  ],
                  data?.sasaran_vaksinasi_lansia
                )
              : 0
          }
          value={
            data?.tahapan_vaksinasi.lansia[tahap === 1 ? 'total_vaksinasi1' : 'total_vaksinasi2']
          }
          total={data?.sasaran_vaksinasi_lansia}
          divaksin={data?.tahapan_vaksinasi.lansia[tahap === 1 ? 'sudah_vaksin1' : 'sudah_vaksin2']}
          tertunda={
            data?.tahapan_vaksinasi.lansia[tahap === 1 ? 'tertunda_vaksin1' : 'tertunda_vaksin2']
          }
        />
      </BoxWrapper>
    </Box>
  </Box>
));

export interface VaccinateSectionBlockProps {
  data?: VaksinStats;
  error: boolean;
  timestamp?: Date;
}

const VaccinateSectionBlock: React.FC<VaccinateSectionBlockProps> = React.memo(
  ({ data, error, timestamp }) => {
    if (error) {
      Sentry.withScope(scope => {
        scope.setTag('api_error', 'tahapan_vaksin');
        Sentry.captureException(error);
      });
    }
    const SectionHeading = styled(Heading)`
      margin-bottom: ${themeProps.space.md}px;

      ${themeProps.mediaQueries.md} {
        margin-bottom: ${themeProps.space.xl}px;
      }
    `;

    const DetailsWrapper = styled(Box)`
      ${themeProps.mediaQueries.md} {
        display: flex;
        align-items: flex-end;
        justify-content: space-between;
      }
    `;

    return (
      <Stack mb="xxl" mt={50}>
        <SectionHeading variant={800} as="h2">
          Progress Vaksinasi COVID-19
        </SectionHeading>

        <Box>
          <GridWrapper>
            <ProgressBox tahap={1} data={data} />
            <ProgressBox tahap={2} data={data} />
          </GridWrapper>
          <DetailsWrapper>
            <Box mt="md">
              <Text as="h5" m={0} variant={200} color="accents04" fontWeight={400}>
                {error ? ERROR_TITLE : 'Pembaruan Terakhir'}
              </Text>
              <Text as="p" variant={400} color="accents07" fontFamily="monospace">
                {timestamp ? formatTime(new Date(timestamp), 'longest') : ERROR_MESSAGE}
              </Text>
            </Box>
          </DetailsWrapper>
        </Box>
      </Stack>
    );
  }
);

const VaccinateSection: React.FC = () => {
  const { data, error } = useSWR<VaksinResponse | undefined>(CEKDIRI_API_URL, fetch);
  const clear = data?.monitoring[data.monitoring.length - 1];
  return <VaccinateSectionBlock data={clear} timestamp={data?.last_updated} error={!!error} />;
};

export default VaccinateSection;
