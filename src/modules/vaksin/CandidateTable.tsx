import * as React from 'react';
import useSWR from 'swr/dist';
import styled from '@emotion/styled';
import * as Sentry from '@sentry/browser';

import { Box, Heading, themeProps, Text, Stack, Table } from 'components/design-system';

import { fetch } from 'utils/api';
import formatTime from 'utils/formatTime';

import { KandidatResponse, Kandidat, KANDIDAT_VAKSIN_API_URL } from 'types/vaksin';

// const EMPTY_DASH = '----';
const ERROR_TITLE = 'Error - Gagal mengambil data terbaru';
const ERROR_MESSAGE = 'Gagal mengambil data. Mohon coba lagi dalam beberapa saat.';

function color(status?: number) {
  if (status === 1) {
    return 'brandred';
  }
  if (status === 2) {
    return 'warning02';
  }
  if (status === 3) {
    return 'chart';
  }
  if (status === 4) {
    return 'primary02';
  }
  if (status === 5) {
    return 'highlight03';
  }
  if (status === 6) {
    return 'success02';
  }
  return 'accents08';
}

const Td = styled('td')`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export interface TableProps {
  data?: KandidatResponse;
}

export interface DataProps {
  data?: Kandidat;
}

const Tr: React.FC<DataProps> = React.memo(({ data }) => (
  <tr>
    <Td>
      <Box width={8} height={8} borderRadius={8} backgroundColor={color(data?.faseStatus)} mr={5} />
      {data?.fase}
    </Td>
    <td>{data?.dibuatOleh}</td>
    <td>{data?.tipeVaksin}</td>
  </tr>
));

const TableBox: React.FC<TableProps> = React.memo(({ data }) => (
  <Box
    display="flex"
    px="xl"
    pt="xl"
    pb="xl"
    overflow="auto"
    borderRadius={6}
    backgroundColor="card"
  >
    <Table>
      <thead>
        <tr>
          <th>Tahapan</th>
          <th>Dibuat oleh</th>
          <th>Tipe Vaksin</th>
        </tr>
      </thead>
      <tbody>
        {data?.allKandidatVaksins.map(item => (
          <Tr key={item.id} data={item} />
        ))}
      </tbody>
    </Table>
  </Box>
));

export interface ResearchSectionBlockProps {
  data?: KandidatResponse;
  error?: boolean;
}

const ResearchSectionBlock: React.FC<ResearchSectionBlockProps> = React.memo(({ data, error }) => {
  if (error) {
    Sentry.withScope(scope => {
      scope.setTag('api_error', 'kandidat_vaksin');
      Sentry.captureException(error);
    });
  }
  const SectionHeading = styled(Heading)`
    margin-bottom: ${themeProps.space.md}px;

    ${themeProps.mediaQueries.md} {
      margin-bottom: ${themeProps.space.xl}px;
    }
  `;

  const DetailsWrapper = styled(Box)`
    ${themeProps.mediaQueries.md} {
      display: flex;
      align-items: flex-end;
      justify-content: space-between;
    }
  `;

  return (
    <Stack mb="xxl" mt={50}>
      <SectionHeading variant={800} as="h2">
        Kandidat Vaksin COVID-19 teratas
      </SectionHeading>

      <Box>
        <TableBox data={data} />
        <DetailsWrapper>
          <Box mt="md">
            <Text as="h5" m={0} variant={200} color="accents04" fontWeight={400}>
              {error ? ERROR_TITLE : 'Pembaruan Terakhir'}
            </Text>
            <Text as="p" variant={400} color="accents07" fontFamily="monospace">
              {data?.lastUpdate ? formatTime(new Date(data?.lastUpdate), 'longest') : ERROR_MESSAGE}
            </Text>
          </Box>
        </DetailsWrapper>
      </Box>
    </Stack>
  );
});

const ResearchSection: React.FC = () => {
  const { data, error } = useSWR<KandidatResponse | undefined>(KANDIDAT_VAKSIN_API_URL, fetch);
  return <ResearchSectionBlock data={data} error={!!error} />;
};

export default ResearchSection;
